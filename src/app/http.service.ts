import { IData } from 'src/app/app.types';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  url="https://saral-dev-api.anuvaad.org/getMarksReport";
  constructor(private http:HttpClient) { }

  getStudents():Observable <IData[]> {
    return this.http.get<IData[]>(this.url);
  }
}
