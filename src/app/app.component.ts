import { IData } from 'src/app/app.types';
import { HttpService } from './http.service';
import { Component,OnInit } from '@angular/core';
// import { IData, IStudent,IMarks } from './app.types';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{

  constructor(private httpservice:HttpService) { }

  studentInfo:any;
  infoData1:Array<Object> = [];
  all:number = 0;
  partial:number = 0;
  none:number = 0;


  ngOnInit(){
    this.httpservice.getStudents().subscribe(data => {
      this.studentInfo =data;
      for(let student of this.studentInfo.data){
        let count = 0;
        let length = 0;
        for(let item of student.marksInfo){
          if(item.obtainedMarks == 1){
            count += 1;
          }
          length += 1;
        }
        if(count===length){
          this.all += 1;
        }else if (count === 0){
          this.none += 1;
        }else{
          this.partial += 1;
        }
      }
    });

  }
}
