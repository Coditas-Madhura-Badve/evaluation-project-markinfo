export interface IData{
    data:string,
    info:IStudent
}

export interface IStudent{
    examDate: Date,
	studentIdTrainingData: string[],
	predictionConfidence: [],
	studentAvailability: boolean,
	maxMarksTrainingData: string[],
	maxMarksConfidence: string[],
	obtainedMarksTrainingData: string[],
	obtainedMarksConfidence: [],
	createdOn: number,
	_id: number,
	section: string,
	studentId: number,
	securedMarks: number,
    totalMarks: number,
	marksInfo:IMarks
}

export interface IMarks{
    predictionConfidence: string[],
	tags: string[],
	trainingData: string[],
	questionId: string,
	obtainedMarks: number
}